QT += opengl declarative testlib

TARGET = tst_performance

SOURCES += \
    tst_performance.cpp \
    ../../../src/shadereffectitem.cpp \
    ../../../src/shadereffectsource.cpp \
    ../../../src/shadereffect.cpp \
    ../../../src/shadereffectbuffer.cpp \
    ../../../src/scenegraph/qsggeometry.cpp

HEADERS += \
    ../../../src/shadereffectitem.h \
    ../../../src/shadereffectsource.h \
    ../../../src/shadereffect.h \
    ../../../src/shadereffectbuffer.h \
    ../../../src/scenegraph/qsggeometry.h

OTHER_FILES += \
    *.qml \
    *.png \
    *.jpg
