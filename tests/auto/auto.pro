load(qttest_p4)

QT += opengl declarative
SOURCES += tst_qmlshadersplugin.cpp

SOURCES += \
    ../../src/shadereffectitem.cpp \
    ../../src/shadereffectsource.cpp \
    ../../src/shadereffect.cpp \
    ../../src/shadereffectbuffer.cpp \
    ../../src/scenegraph/qsggeometry.cpp

HEADERS += \
    ../../src/shadereffectitem.h \
    ../../src/shadereffectsource.h \
    ../../src/shadereffect.h \
    ../../src/shadereffectbuffer.h \
    ../../src/scenegraph/qsggeometry.h

OTHER_FILES += main.qml
