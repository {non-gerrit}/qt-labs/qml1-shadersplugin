QT += declarative opengl

# Add more folders to ship with the application, here
folder_01.source = qml/helloworld
folder_01.target = qml
DEPLOYMENTFOLDERS = folder_01

# Additional import path used to resolve Qml modules in Creator's code model
QML_IMPORT_PATH =

# Avoid auto screen rotation
#DEFINES += ORIENTATIONLOCK

# Needs to be defined for Symbian
#DEFINES += NETWORKACCESS

symbian:TARGET.UID3 = 0xE5022B60

# Define QMLJSDEBUGGER to enable basic debugging (setting breakpoints etc)
# Define QMLOBSERVER for advanced features (requires experimental QmlInspector plugin!)
#DEFINES += QMLJSDEBUGGER
#DEFINES += QMLOBSERVER

# The .cpp file which was generated for your project. Feel free to hack it.
SOURCES += main.cpp

symbian {
    DEFINES += OPENGL_GRAPHICSSYSTEM
}

unix:!macx:!symbian {
    SOURCES += qmlapplicationviewer/qmlapplicationviewer.cpp
    HEADERS += qmlapplicationviewer/qmlapplicationviewer.h
    INCLUDEPATH += qmlapplicationviewer
    maemo5 {
        desktop.path = /usr/share/applications/hildon
    } else {
        desktop.path = /usr/share/applications
    }
    desktop.files = $${TARGET}.desktop
    icon.files = $${TARGET}.png
    icon.path = /usr/share/icons/hicolor/64x64/apps
    target.path = /usr/bin
    resourcefiles.files = qml/$${TARGET}/*
    resourcefiles.path = /usr/share/$${TARGET}/qml/$${TARGET}

    INSTALLS += target desktop icon resourcefiles
} else {
# Please do not modify the following two lines. Required for deployment.
include(qmlapplicationviewer/qmlapplicationviewer.pri)
qtcAddDeployment()
}
